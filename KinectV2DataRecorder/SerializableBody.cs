﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KinectV2DataRecorder
{
    [DataContract]
    public class SerializableBody
    {
        //     Gets the edges of the field of view that clip the body.
        [DataMember]
        public FrameEdges ClippedEdges { get; set;  }
        //     Gets the confidence of the body's left hand state.
        [DataMember]
        public TrackingConfidence HandLeftConfidence { get; set; }
        //     Gets the status of the body's left hand state.
        [DataMember]
        public HandState HandLeftState { get; set; }
        //     Gets the confidence of the body's right hand state.
        [DataMember]
        public TrackingConfidence HandRightConfidence { get; set; }
        //     Gets the status of the body's right hand state.
        [DataMember]
        public HandState HandRightState { get; set; }
        //     Gets whether or not the body is restricted.
        [DataMember]
        public bool IsRestricted { get; set; }
        //     Gets whether or not the body is tracked.
        [DataMember]
        public bool IsTracked { get; set; }
        //     Gets the number of joints in a body.
        [DataMember]
        public static int JointCount { get; set; }
        //     Gets the joint orientations of the body.
        [DataMember]
        public Dictionary<JointType, JointOrientation> JointOrientations { get; set; }
        //     Gets the joint positions of the body.
        [DataMember]
        public Dictionary<JointType, Joint> Joints { get; set; }
        //      Gets the joint mapped positions of the body.
        [DataMember]
        public Dictionary<JointType, Point> MappedJoints { get; set;  }
        //     Gets the lean vector of the body.
        [DataMember]
        public PointF Lean { get; set; }
        //     Gets the tracking state for the body lean.
        [DataMember]
        public TrackingState LeanTrackingState { get; set; }
        //     Gets the tracking ID for the body.
        [DataMember]
        public ulong TrackingId { get; set; }

        public SerializableBody() { }

        public SerializableBody(Body body)
        {
            this.ClippedEdges = body.ClippedEdges;
            this.HandLeftConfidence = body.HandLeftConfidence;
            this.HandLeftState = body.HandLeftState;
            this.HandRightConfidence = body.HandRightConfidence;
            this.HandRightState = body.HandRightState;
            this.IsRestricted = body.IsRestricted;
            this.IsTracked = body.IsTracked;
            SerializableBody.JointCount = Body.JointCount;
            this.JointOrientations = new Dictionary<JointType, JointOrientation>();
            foreach (var kv in body.JointOrientations)this.JointOrientations.Add(kv.Key, kv.Value);
            this.Joints = new Dictionary<JointType, Joint>();
            foreach(var kv in body.Joints) this.Joints.Add(kv.Key, kv.Value);
            this.Lean = body.Lean;
            this.LeanTrackingState = body.LeanTrackingState;
            this.TrackingId = body.TrackingId;
        }
    }
}
