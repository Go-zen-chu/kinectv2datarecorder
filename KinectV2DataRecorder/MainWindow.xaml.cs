﻿using Microsoft.Kinect;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace KinectV2DataRecorder
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        // INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        public event PropertyChangedEventHandler PropertyChanged;
        
        private KinectSensor kinectSensor = null;
        // Size of the RGB pixel in the bitmap
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        public const ushort KINECT_VALID_MAX_DEPTH = 10000;
        // Lool up table for converting depth to color
        private readonly byte[] lut_g = new byte[KINECT_VALID_MAX_DEPTH];
        private readonly byte[] lut_b = new byte[KINECT_VALID_MAX_DEPTH];
        
        
        #region Recording related variables
        private bool isRecording = false;
        private DateTime prevRecordedTime = DateTime.MinValue;

        string colorImagesSavingDirPath;
        string depthImagesSavingDirPath;
        string bodyDataSavingDirPath;
        #endregion

        // for calculating FPS
        private DateTime prevRenderedTime = DateTime.MinValue;
        // Coordinate mapper to map one type of point to another
        private CoordinateMapper coordinateMapper = null;
        // Reader for depth/color/body index frames
        private MultiSourceFrameReader multiFrameSourceReader = null;

        #region Depth related variables
        // Description of the data contained in the depth frame
        private FrameDescription depthFrameDescription = null;
        // Intermediate storage for receiving depth frame data from the sensor
        private ushort[] depthFrameData = null;
        // Intermediate storage for frame data converted to color 
        private byte[] depthPixels = null;
        // depth mapped points
        private DepthSpacePoint[] depthPoints = null;
        #endregion

        Body[] bodies = null;

        #region Properties
        private WriteableBitmap colorBitmap = null;
        public ImageSource ColorImageSource
        {
            get
            {
                return this.colorBitmap;
            }
        }

        private WriteableBitmap depthBitmap = null;
        public ImageSource DepthImageSource
        {
            get
            {
                return this.depthBitmap;
            }
        }

        int recordingFps = 500; // = 500ms
        public int RecordingFPS
        {
            get { return recordingFps; }
            set { recordingFps = value; }
        }

        private int kinect_fps = 0;
        public int FPS
        {
            get { return kinect_fps; }
            set
            {
                if (this.kinect_fps != value)
                {
                    this.kinect_fps = value;
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("FPS"));
                    }
                }
            }
        }

        private string statusText = null;
        // Gets or sets the current status text to display
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }
        #endregion

        public MainWindow()
        {
            try
            {
                // get the kinectSensor object
                this.kinectSensor = KinectSensor.GetDefault();

                this.multiFrameSourceReader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Depth | FrameSourceTypes.Color | FrameSourceTypes.Body);
                // wire handler for frames arrival
                this.multiFrameSourceReader.MultiSourceFrameArrived += this.Reader_MultiSourceFrameArrived;
                // get the coordinate mapper
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;

                #region Color initialization
                // create the colorFrameDescription from the ColorFrameSource using Bgra format
                var colorFrameDescription = this.kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);
                int colorWidth = colorFrameDescription.Width;
                int colorHeight = colorFrameDescription.Height;
                // create the bitmap to display
                this.colorBitmap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
                #endregion

                #region Depth initialization
                // get FrameDescription from DepthFrameSource
                this.depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
                // allocate space to put the pixels being received and converted
                this.depthFrameData = new ushort[depthFrameDescription.Width * depthFrameDescription.Height];
                // allocate space to put the pixels being received and converted
                this.depthPixels = new byte[colorWidth * colorHeight * this.bytesPerPixel];
                // create the bitmap to display
                this.depthBitmap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
                this.depthPoints = new DepthSpacePoint[colorWidth * colorHeight];
                #endregion

                // set IsAvailableChanged event notifier
                this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;
                // open the sensor
                this.kinectSensor.Open();
                // set the status text
                this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText : Properties.Resources.NoSensorStatusText;

            }
            catch(Exception ex)
            {
                // Fails if kinect v2 is not connected 
                System.Windows.MessageBox.Show(ex.Message, ex.GetType().ToString());
                return;
            }
            // use the window object as the view model in this simple example
            this.DataContext = this;
            // initialize the components (controls) of the window
            this.InitializeComponent();

            if (Directory.Exists(Properties.Settings.Default.MainWindow_RecordedDataRootPath) == false)
                startRecordingToggleButton.IsEnabled = false;

            // Create LUT
            for (ushort depth = 0; depth < KINECT_VALID_MAX_DEPTH; depth++)
            {
                lut_g[depth] = (byte)(depth / 256);
                lut_b[depth] = (byte)(depth % 256);
            }
        }
        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText : Properties.Resources.NoSensorStatusText;
        }
        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.multiFrameSourceReader != null)
            {
                // MultiSourceFrameReder is IDisposable
                this.multiFrameSourceReader.Dispose();
                this.multiFrameSourceReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        /// <summary>
        /// Handles the depth/color/body index frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            int colorWidth = 0, colorHeight = 0;
            int depthWidth = 0, depthHeight = 0;

            bool colorFrameProcessed = false;
            bool depthFrameProcessed = false;
            bool bodyFrameProcessed = false;

            MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();
            if (multiSourceFrame == null) return;

            #region Prepare color image 
            using (ColorFrame colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame())
            {
                if (colorFrame != null)
                {
                    FrameDescription colorFrameDescription = colorFrame.FrameDescription;
                    using (KinectBuffer colorBuffer = colorFrame.LockRawImageBuffer())
                    {
                        this.colorBitmap.Lock();
                        colorWidth = colorFrameDescription.Width;
                        colorHeight = colorFrameDescription.Height;
                        // verify data and write the new color frame data to the display bitmap
                        if ((colorWidth == this.colorBitmap.PixelWidth) && (colorHeight == this.colorBitmap.PixelHeight))
                        {
                            colorFrame.CopyConvertedFrameDataToIntPtr(
                                this.colorBitmap.BackBuffer,
                                (uint)(colorWidth * colorHeight * 4),
                                ColorImageFormat.Bgra);

                            this.colorBitmap.AddDirtyRect(new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight));
                            // flag on
                            colorFrameProcessed = true;
                        }
                        this.colorBitmap.Unlock();
                    }
                }
            } 
            #endregion

            #region Obtain Depth data
            // Frame Acquisition should always occur first when using multiSourceFrameReader
            using (var depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
            {
                if (depthFrame != null)
                {
                    var depthFrameDescription = depthFrame.FrameDescription;
                    depthWidth = depthFrameDescription.Width;
                    depthHeight = depthFrameDescription.Height;

                    if ((depthWidth * depthHeight) == this.depthFrameData.Length)
                    {
                        depthFrame.CopyFrameDataToArray(this.depthFrameData);
                        // flag on
                        depthFrameProcessed = true;
                    }
                }
            } 
            #endregion

            #region Prepare Skeleton
            using (var bodyFrame = multiSourceFrame.BodyFrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    bodies = new Body[bodyFrame.BodyCount];
                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);

                    bodyFrameProcessed = true;
                }
            }

            #endregion

            // calculate fps
            var currentTime = DateTime.Now;
            this.FPS = (int)(1000 / (currentTime - prevRenderedTime).TotalMilliseconds);
            prevRenderedTime = currentTime;

            #region Prepare Depth image
            if (colorFrameProcessed && depthFrameProcessed)
            {
                // map depth to color image
                this.coordinateMapper.MapColorFrameToDepthSpace(this.depthFrameData, this.depthPoints);
                Parallel.For(0, colorHeight, row =>
                {
                    int colorIdx = colorWidth * row;
                    // set source for copy to the color pixel
                    int sourceIdx = colorIdx * this.bytesPerPixel;
                    for (int col = 0; col < colorWidth; ++col)
                    {
                        DepthSpacePoint depthPoint = this.depthPoints[colorIdx];

                        if (!float.IsNegativeInfinity(depthPoint.X) && !float.IsNegativeInfinity(depthPoint.Y))
                        {
                            // make sure the depth pixel maps to a valid point in color space
                            int depthX = (int)(depthPoint.X + 0.5f);
                            int depthY = (int)(depthPoint.Y + 0.5f);
                            if ((depthX >= 0) && (depthX < depthWidth) && (depthY >= 0) && (depthY < depthHeight))
                            {
                                int depthIndex = (depthY * depthWidth) + depthX;
                                var depth = depthFrameData[depthIndex];
                                if (depth <= KINECT_VALID_MAX_DEPTH)
                                {
                                    this.depthPixels[sourceIdx++] = lut_b[depth]; // b
                                    this.depthPixels[sourceIdx++] = lut_g[depth]; // g
                                    this.depthPixels[sourceIdx++] = 0; // r
                                    this.depthPixels[sourceIdx++] = 0xff; // a
                                }
                                else
                                {
                                    // Set invalid values to red
                                    this.depthPixels[sourceIdx++] = 0; // b
                                    this.depthPixels[sourceIdx++] = 0; // g
                                    this.depthPixels[sourceIdx++] = 0xff; // r
                                    this.depthPixels[sourceIdx++] = 0xff; // a
                                }
                            }
                        }
                        else
                        {
                            // Set invalid values to white
                            this.depthPixels[sourceIdx++] = 0xff; // b
                            this.depthPixels[sourceIdx++] = 0xff; // g
                            this.depthPixels[sourceIdx++] = 0xff; // r
                            this.depthPixels[sourceIdx++] = 0xff; // a
                        }
                        colorIdx++;
                    }
                });

                this.depthBitmap.WritePixels(
                    new Int32Rect(0, 0, this.depthBitmap.PixelWidth, this.depthBitmap.PixelHeight),
                    this.depthPixels, depthBitmap.PixelWidth * bytesPerPixel, 0);
            }
            #endregion

            #region Map Body to Color Image

            var mappedJointPosDicts = new Dictionary<ulong, Dictionary<JointType, Point>>();
            bodyCanvas.Children.Clear();
            if (bodyFrameProcessed && recordSkeletonCheckBox.IsChecked.Value)
            {
                foreach (var body in bodies)
                {
                    if (body.IsTracked)
                    {
                        var mappedJointPosDict = MapBodyToScreen(coordinateMapper, body);
                        DrawBonesAndJoints(mappedJointPosDict, bodyCanvas);
                        mappedJointPosDicts.Add(body.TrackingId, mappedJointPosDict);
                    }
                }
            }

            #endregion

            if (isRecording && (currentTime - prevRecordedTime).TotalMilliseconds > recordingFps)
            {
                #region Save data
                prevRecordedTime = currentTime;
                var currentTimeStr = currentTime.ToString(Properties.Resources.DateTimeFormatText);

                if (colorFrameProcessed && recordColorCheckBox.IsChecked.Value)
                {
                    // Saving Color image Data
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(this.colorBitmap));
                    var colorImgPath = Path.Combine(colorImagesSavingDirPath, currentTimeStr + ".png");
                    using (var fs = new FileStream(colorImgPath, FileMode.Create)) encoder.Save(fs);
                }

                if (depthFrameProcessed && recordDepthCheckBox.IsChecked.Value)
                {
                    // Saving Depth Data
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(this.depthBitmap));
                    var depthImgPath = Path.Combine(depthImagesSavingDirPath, currentTimeStr + ".png");
                    using (var fs = new FileStream(depthImgPath, FileMode.Create)) encoder.Save(fs);
                }


                if (bodyFrameProcessed && recordSkeletonCheckBox.IsChecked.Value)
                {
                    var bodyDataDirPath = Path.Combine(bodyDataSavingDirPath, currentTimeStr);
                    foreach (var body in bodies)
                    {
                        if (body.IsTracked)
                        {
                            Directory.CreateDirectory(bodyDataDirPath);
                            var serializableBody = new SerializableBody(body);
                            serializableBody.MappedJoints = mappedJointPosDicts[body.TrackingId];
                            ExportDataJson<SerializableBody>(Path.Combine(bodyDataDirPath, body.TrackingId + ".json"), serializableBody);
                        }
                    }
                }
                #endregion
            }
        }
        
        private Dictionary<JointType, Point> MapBodyToScreen(CoordinateMapper coordinateMapper, Body body)
        {
            var mappedJointPosDict = new Dictionary<JointType, Point>();
            foreach (var kv in body.Joints)
            {
                var cameraPos = kv.Value.Position;
                // sometimes the depth(Z) of an inferred joint may show as negative
                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                if(cameraPos.Z <= 0) cameraPos.Z = 0.1f;
                var colorSpacePoint = coordinateMapper.MapCameraPointToColorSpace(cameraPos);
                mappedJointPosDict.Add(kv.Key, new Point(colorSpacePoint.X, colorSpacePoint.Y));
            }
            return mappedJointPosDict;
        }

        private void DrawBonesAndJoints(Dictionary<JointType, Point> mappedJointPosDict, Canvas canvas)
        {
            // Render Torso
            DrawBone(mappedJointPosDict, canvas, JointType.Head, JointType.Neck);
            DrawBone(mappedJointPosDict, canvas, JointType.Neck, JointType.SpineShoulder);
            DrawBone(mappedJointPosDict, canvas, JointType.SpineShoulder, JointType.SpineMid);
            DrawBone(mappedJointPosDict, canvas, JointType.SpineMid, JointType.SpineBase);
            DrawBone(mappedJointPosDict, canvas, JointType.SpineShoulder, JointType.ShoulderRight);
            DrawBone(mappedJointPosDict, canvas, JointType.SpineShoulder, JointType.ShoulderLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.SpineBase, JointType.HipRight);
            DrawBone(mappedJointPosDict, canvas, JointType.SpineBase, JointType.HipLeft);

            // Left Arm
            DrawBone(mappedJointPosDict, canvas, JointType.ShoulderLeft, JointType.ElbowLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.ElbowLeft, JointType.WristLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.WristLeft, JointType.HandLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.HandLeft, JointType.HandTipLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.WristLeft, JointType.ThumbLeft);

            // Right Arm
            DrawBone(mappedJointPosDict, canvas, JointType.ShoulderRight, JointType.ElbowRight);
            DrawBone(mappedJointPosDict, canvas, JointType.ElbowRight, JointType.WristRight);
            DrawBone(mappedJointPosDict, canvas, JointType.WristRight, JointType.HandRight);
            DrawBone(mappedJointPosDict, canvas, JointType.HandRight, JointType.HandTipRight);
            DrawBone(mappedJointPosDict, canvas, JointType.WristRight, JointType.ThumbRight);

            // Left Leg
            DrawBone(mappedJointPosDict, canvas, JointType.HipLeft, JointType.KneeLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.KneeLeft, JointType.AnkleLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.AnkleLeft, JointType.FootLeft);

            // Right Leg
            DrawBone(mappedJointPosDict, canvas, JointType.HipRight, JointType.KneeRight);
            DrawBone(mappedJointPosDict, canvas, JointType.KneeRight, JointType.AnkleRight);
            DrawBone(mappedJointPosDict, canvas, JointType.AnkleRight, JointType.FootRight);
        }

        private void DrawBone(Dictionary<JointType, Point> mappedJointPosDict, Canvas canvas, JointType jointType0, JointType jointType1, Brush brush = null, double thickness = 3)
        {
            if (mappedJointPosDict.ContainsKey(jointType0) == false || mappedJointPosDict.ContainsKey(jointType1) == false) return;
            var p0 = mappedJointPosDict[jointType0];
            var p1 = mappedJointPosDict[jointType1];
            if (double.IsInfinity(p0.X) || double.IsInfinity(p0.Y) || double.IsInfinity(p1.X) || double.IsInfinity(p1.Y)) return;
            if (brush == null) brush = Brushes.LightGreen;
            var line = new System.Windows.Shapes.Line() { Stroke = brush, StrokeThickness = thickness, X1 = p0.X, Y1 = p0.Y, X2 = p1.X, Y2 = p1.Y };
            canvas.Children.Add(line);
        }

        private void recordDataCheckBox_Click(object sender, RoutedEventArgs e)
        {
            var checkbox = sender as System.Windows.Controls.CheckBox;
            if (checkbox == null) return;
            Image targetImage = null;
            if (checkbox.Name == recordColorCheckBox.Name) targetImage = colorImage;
            else if (checkbox.Name == recordDepthCheckBox.Name) targetImage = depthImage;

            if (targetImage == null) return;
            if (checkbox.IsChecked.Value) targetImage.Visibility = System.Windows.Visibility.Visible;
            else targetImage.Visibility = System.Windows.Visibility.Hidden;
        }

        private void dataViewButton_Click(object sender, RoutedEventArgs e)
        {
            var dataViewWindow = new DataViewWindow(Properties.Settings.Default.MainWindow_RecordedDataRootPath);
            dataViewWindow.Show();
        }

        /// <summary>
        /// Handles the user clicking on the screenshot button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void startRecordingToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (startRecordingToggleButton == null) return; // GUI is not initialized yet
            if (startRecordingToggleButton.IsChecked.Value) // Start Recording
            {
                isRecording = true;
                startRecordingToggleButton.Content = Properties.Resources.StopRecordingDataText;
                startRecordingToggleButton.Background = Brushes.Coral;

                var startRecordingSessionTime = DateTime.Now; // Time when recording has started
                var appRootPath = Properties.Settings.Default.MainWindow_RecordedDataRootPath;
                var startRecordingSessionTimeStr = startRecordingSessionTime.ToString(Properties.Resources.DateTimeFormatText);
                colorImagesSavingDirPath = Path.Combine(appRootPath, Properties.Resources.DirColorImages, startRecordingSessionTimeStr);
                depthImagesSavingDirPath = Path.Combine(appRootPath, Properties.Resources.DirDepthImages, startRecordingSessionTimeStr);
                bodyDataSavingDirPath = Path.Combine(appRootPath, Properties.Resources.DirSkeleton, startRecordingSessionTimeStr);
                Directory.CreateDirectory(colorImagesSavingDirPath);
                Directory.CreateDirectory(depthImagesSavingDirPath);
                Directory.CreateDirectory(bodyDataSavingDirPath);
                recordingFPSTextBox.IsEnabled = false;
            }
            else // End Recording
            {
                isRecording = false;
                startRecordingToggleButton.Content = Properties.Resources.StartRecordingDataText;
                startRecordingToggleButton.Background = Brushes.LightGreen;
                recordingFPSTextBox.IsEnabled = true;
            }
        }

        private void selectRecordedDataExportPathButton_Click(object sender, RoutedEventArgs e)
        {
            var fbd = new FolderBrowserDialog();
            fbd.Description = "Choose Recorded Data Export Path";
            if (Directory.Exists(Properties.Settings.Default.MainWindow_RecordedDataRootPath) == false)
            {
                fbd.SelectedPath = Properties.Settings.Default.MainWindow_RecordedDataRootPath;
            }
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Directory.Exists(fbd.SelectedPath) == false) return;
                recordedDataExportPathTextBox.Text = fbd.SelectedPath;
                Properties.Settings.Default.MainWindow_RecordedDataRootPath = fbd.SelectedPath;
                Properties.Settings.Default.Save();
                startRecordingToggleButton.IsEnabled = true;
            }
        }

        public static void ExportDataJson<T>(string exportPath, T obj)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            using (var fs = new FileStream(exportPath, FileMode.Create, FileAccess.Write))
                serializer.WriteObject(fs, obj);
        }
        public static T ImportJsonData<T>(string importPath)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            using (var fs  = new FileStream(importPath, FileMode.Open, FileAccess.Read))
                return (T)serializer.ReadObject(fs);
        }
    
    }
}
